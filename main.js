import mysql from 'mysql2';
import express from 'express';

import session from 'express-session';

import path from 'path';

const __dirname = path.resolve();
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nodelogin'
});
const app = express();
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', function (request, response) {
    // Render login template
    if (typeof request.session.loggedin !== 'undefined') {
        response.redirect('/home');
    } else {
        response.sendFile(path.join(__dirname + '/login.html'));
    }
});
//xử lý login
app.post('/auth', function (request, response) {
    let username = request.body.username;
    let password = request.body.password;
    if (username && password) {
        connection.query('SELECT * FROM accounts WHERE username=? AND password=?', [username, password], function (error, results, fields) {
            if (error) throw error;
            if (results.length > 0) {
                request.session.loggedin = true;
                request.session.username = username;
                response.redirect('/home');
            } else {
                response.send('Incorrect Username and/or Password!');
            }
            response.end();
        });
    } else {
        response.send('Please enter Username and Password!');
        response.end();
    }
});
app.get('/home', function (request, response) {
    if (typeof request.session.loggedin !== 'undefined') {
        response.send('Welcome back, ' + request.session.username + '!');
    } else {
        response.redirect('/');
    }
    response.end();
});
app.listen(3000);